# in this file, i will get the singger vocal audio from the pkl dataset
# depend on ffmpeg it need add the untrust ppa to ubuntu
# sudo add-apt-repository ppa:mc3man/trusty-media
# sudo apt-get update
# sudo apt-get dist-upgrade
__author__ = 'zhangxulong'
# use for collect songs but here we use a dataset and this code will be the util to handle the dataset.
import cPickle
import os
import os.path
import numpy
import wave

from feature_extraction import feature_reduction_union_list


def load_audio(song_dir):
    file_format = song_dir.split('.')[1]
    if 'wav' == file_format:
        song = wave.open(song_dir, "rb")
        params = song.getparams()
        nchannels, samplewidth, framerate, nframes = params[:4]  # format info
        song_data = song.readframes(nframes)
        song.close()
        wave_data = numpy.fromstring(song_data, dtype=numpy.short)
        wave_data.shape = -1, 2
        wave_data = wave_data.T
    else:
        raise NameError("now just support wav format audio files")
    return wave_data


def pickle_dataset(dataset, out_pkl='dataset.pkl'):
    pkl_file = file(out_pkl, 'wb')
    cPickle.dump(dataset, pkl_file, True)
    pkl_file.close()
    return 0


def build_song_set(songs_dir):
    songs_dataset = []
    for parent, dirnames, filenames in os.walk(songs_dir):
        for filename in filenames:
            song_dir = os.path.join(parent, filename)
            audio = load_audio(song_dir)
            # change the value as your singer name level in the dir
            # eg. short_wav/new_wav/dataset/singer_name so I set 3
            singer = song_dir.split('/')[1]
            # this value depends on the singer file level in the dir
            songs_dataset.append((audio, singer))
    pickle_dataset(songs_dataset, 'songs_audio_singer.pkl')
    return 0


def build_dataset(dataset_dir):
    dataset = {"author": "zhangxulong"}
    dict_singer_label = {"aerosmith": 0, "beatles": 1, "creedence_clearwater_revival": 2, "cure": 3,
                         "dave_matthews_band": 4, "depeche_mode": 5, "fleetwood_mac": 6, "garth_brooks": 7,
                         "green_day": 8, "led_zeppelin": 9, "madonna": 10, "metallica": 11, "prince": 12, "queen": 13,
                         "radiohead": 14, "roxette": 15, "steely_dan": 16, "suzanne_vega": 17, "tori_amos": 18,
                         "u2": 19}
    singers = []
    singers_label = []
    song_feature = []
    for parent, dirnames, filenames in os.walk(dataset_dir):
        for filename in filenames:
            song_dir = os.path.join(parent, filename)
            song_feature = feature_reduction_union_list(song_dir)
            # song = load_audio(song_dir)
            song_feature.append(song_feature)
            # change the value as your singer name level in the dir
            # eg. short_wav/new_wav/dataset/singer_name so I set 3
            singer = song_dir.split('/')[1]
            # this value depends on the singer file level in the dir
            singers.append(singer)
            singers_label.append(dict_singer_label[singer])
    dataset['singers'] = singers
    dataset['data'] = numpy.array(song_feature)
    dataset['singers_label'] = numpy.array(singers_label)
    dataset['dict_singer_label'] = dict_singer_label
    pickle_dataset(dataset)
    return 0


def load_data(pkl_dir='dataset.pkl'):
    pkl_dataset = open(pkl_dir, 'rb')
    dataset = cPickle.load(pkl_dataset)
    return dataset


def test():
    build_dataset('dataset_short5')
    return 0


test()
