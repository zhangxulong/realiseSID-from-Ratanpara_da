# realiseSID-from-Ratanpara
realiseSID-from-Ratanpara
## SONG PREPOSSESS
1.song_collection
  in this file are the main util for handling the music audio dataset into a pickle file
	# use the artist20 dataset there are mp3 about 681MB
	# change it to wav file & run it takes time:455.00 & the wav dataset about 7.5GB
	# change it to mono wav file & run it takes time:212.00 & the mono dataset about 3.7GB
	# change it to vocal-only wav file @ here use matlab do vocal split but here should generate a file list in python
	    # in the matlab it takes 19 hours and the final dataset is with both A & E where A is background and E is the vocal & the matlab dataset about 7.5G
	# remove the background A and we just need the vocal one & run it takes time:0.00 and the dataset about 3.7G
	# change it to short segments wav file &run it takes time:118.00 the dataset about 3.1G
	# now finish the preprocess of the song and we can use it for next steps FEATURE_EXTRACTION
## FEATURE_EXTRACTION

2.song_segmentation
  in this file, i will get the singger vocal audio from the pkl dataset
3.feature_extraction
  extract the feature used for sid
  here used  timbre, pitches and loudness by Echo Nest API 27
  used scikits.talkbox.features.mfcc for 13 mfccs
  and scikits.talkbox.linpred.lpc for 13 lpcs
4.dimension_reduction
5.build_model
6.singer_identification

#
